/***  examples/src/index.js ***/
import React from 'react';
import { render} from 'react-dom';
import { Page, Row, Column } from '../../src';

const App = () => (
    <Page outline>
    	<Row>
    		<Column span={1} justifyContent="flex-start" outline>1</Column>
    		<Column span={1} justifyContent="flex-start" outline>2</Column>
    		<Column span={1} justifyContent="flex-start" outline>3</Column>
    		<Column span={1} justifyContent="flex-start" outline>4</Column>
    		<Column span={1} justifyContent="center" outline>5</Column>
    		<Column span={1} justifyContent="center" outline>6</Column>
    		<Column span={1} justifyContent="center" outline>7</Column>
    		<Column span={1} justifyContent="center" outline>8</Column>
    		<Column span={1} justifyContent="flex-end" outline>9</Column>
    		<Column span={1} justifyContent="flex-end" outline>10</Column>
    		<Column span={1} justifyContent="flex-end" outline>11</Column>
    		<Column span={1} justifyContent="flex-end" outline>12</Column>
    	</Row>

    	<Row>
    		<Column span={3} justifyContent="center" outline>1</Column>
    		<Column span={3} justifyContent="center" outline>2</Column>
    		<Column span={3} justifyContent="center" outline>3</Column>
    		<Column span={3} justifyContent="center" outline>4</Column>
    	</Row>

    	<Row>
    		<Column span={2} justifyContent="center" outline>1</Column>
    		<Column span={2} start={11} justifyContent="center" outline>2</Column>
    	</Row>

    	<Row>
    		<Column span={2} start={5} justifyContent="center" outline>1</Column>
    		<Column span={2} start={7} justifyContent="center" outline>2</Column>
    	</Row>

    	<Row height="100px">
    		<Column alignItems="center" justifyContent="center" outline>1</Column>
    	</Row>

    	<Row height="100px">
    		<Column span={2} start={6} alignItems="center" justifyContent="center" outline>1</Column>
    	</Row>

    	<Row height="100px" alignItems="center">
    		<Column span={2} start={6}  justifyContent="center" outline>1</Column>
    	</Row>
    </Page>
);
render(<App />, document.getElementById("root"));