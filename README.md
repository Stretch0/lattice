# Lattice

Lattice is a tool to help style your React app with Grid and Flexbox layout components

  - Makes use of styled components
  - HTML Grid and Flexbox
  - Responsive
  
### Installation

Install the dependencies and import to your React application.

```sh
$ npm install @stretch/lattice
```

```javascript
import { LayoutProvider, Page, Row, Column } from "@stretch/lattice"

const App = () => (
  <Page>
    <Row span={6}>
      <Column>
        This is left aligned
      </Column>
    </Row>

    <Row span={6}>
      <Column>
        This is right aligned
      </Column>
    </Row>

    <Row span={6} start={6}>
      <Column>
        This is right aligned
      </Column>
    </Row>


  </Page>
)
```

###### Page Component
  - Offers an easy way to set min-height of page to `100vh`
  - Adds display grid with 12 columns by default
  - Easily configurable by passing props or theme the `LayoutProvider`
  
###### Row Component
  - Offers an easy way to create a row which can span 100% or just the columns of parent page or row you want
  - Easily align to start at any column of parent page or row

###### Column Component
  - Offers an easy way to create a Column which can span 100% or just the columns of parent row you want
  - Easily align to start at any column of parent page or row
  - A flexbox component so makes aligning content easy

### Tech

* [Styled Components] - Use the best bits of ES6 and CSS to style your apps without stress!
* [Babel] - Babel is a JavaScript compiler.

### Development

Want to contribute? Great!

Lattice uses Babel + Webpack for fast developing.
Make a change in `./src` and you can play around with examples in `./examples/src` 

Run:
```sh
$ git clone git@gitlab.com:Stretch0/lattice.git
$ cd lattice
$ npm install
$ npm run start
```

License
----

ISC


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [Styled Components]: <https://www.styled-components.com/>
   [Babel]: <https://babeljs.io/>
