import styled from "styled-components"
import { ifProp } from "styled-tools"

const Page = styled("div")`
  min-height: ${({ minHeight, theme: { layout } }) => minHeight || layout.minHeight};
  box-sizing: border-box;
  display: grid;
  grid-template-columns: ${({ theme: { layout }, columns }) => `repeat(${columns || layout.columns}, 1fr)`};
  grid-column-gap: ${({ theme: { layout }, gutter }) => gutter || layout.gutter};
  padding: ${({ theme: { layout }, padding }) => padding || layout.padding};
  grid-auto-rows: min-content;
  overflow: ${({ overflow }) => overflow || "initial"};

  ${ifProp("alignItems", ({ alignItems }) => `align-items: ${alignItems}`)};

  ${ifProp("outline", () => `outline: solid green 1px;`)};

  > * {
    grid-column: 1 / -1;
  }
`

Page.defaultProps = {
  theme: {
    layout: {
      minHeight: "100vh",
      columns: 12,
      gutter: "8px",
      padding: "8px"
    }
  }
}

export default Page
