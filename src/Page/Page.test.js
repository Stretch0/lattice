import React from 'react'
import styled from 'styled-components'
import renderer from 'react-test-renderer'
import Page from "./Page.styles"
import LayoutProvider from "../Provider"
import 'jest-styled-components'

let tree
let props

describe('Page', () => {

	describe("Default styles", () => {
		beforeEach(() => {
			tree = renderer.create(<Page />).toJSON()	
		})
	  
	  it("Should have CSS prop MIN", () => {
	  	expect(tree).toHaveStyleRule('min-height', '100vh')
	  })

	  it("Should have CSS prop BOX", () => {
	  	expect(tree).toHaveStyleRule('box-sizing', 'border-box')
	  })

	  it("Should have CSS prop DISPLAY", () => {
	  	expect(tree).toHaveStyleRule('display', 'grid')
	  })

	  it("Should have CSS prop GRID", () => {
	  	expect(tree).toHaveStyleRule('grid-template-columns', 'repeat(12,1fr)')
	  })

	  it("Should have CSS prop GRID", () => {
	  	expect(tree).toHaveStyleRule('grid-column-gap', '8px')
	  })

	  it("Should have CSS prop PADDING", () => {
	  	expect(tree).toHaveStyleRule('padding', '8px')
	  })

	  it("Should have CSS prop GRID", () => {
	  	expect(tree).toHaveStyleRule('grid-auto-rows', 'min-content')
	  })

	  it("Should have CSS prop OVERFLOW", () => {
	  	expect(tree).toHaveStyleRule('overflow', 'initial')
	  })
	})

	describe("Prop styles", () => {
		beforeEach(() => {
			props = {
				minHeight: "50vh",
				columns: 10,
				gutter: "16px",
				padding: "16px",
				overflow: "hidden",
				alignItems: "center",
				outline: true
			}
			tree = renderer.create(<Page {...props} />).toJSON()	
		})

		it("Should have minHeight", () => {
			expect(tree).toHaveStyleRule("min-height", "50vh")
		})
		it("Should have columns", () => {
			expect(tree).toHaveStyleRule("grid-template-columns", "repeat(10,1fr)")
		})
		it("Should have gutter", () => {
			expect(tree).toHaveStyleRule("grid-column-gap", "16px")
		})
		it("Should have padding", () => {
			expect(tree).toHaveStyleRule("padding", "16px")
		})
		it("Should have overflow", () => {
			expect(tree).toHaveStyleRule("overflow", "hidden")
		})
		it("Should have alignItems", () => {
			expect(tree).toHaveStyleRule("align-items", "center")
		})
		it("Should have outline", () => {
			expect(tree).toHaveStyleRule("outline", "solid green 1px")
		})
	})

	describe("Theme styles", () => {
		beforeEach(() => {
			const theme = {
				layout: {
					minHeight: "40vh",
					columns: 10,
					gutter: "17px",
					padding: "21px"
				}
			}
			tree = renderer.create(
				<LayoutProvider theme={theme}>
					<Page />
				</LayoutProvider>
			).toJSON()	
		})

		it("Should have minHeight", () => {
			expect(tree).toHaveStyleRule("min-height", "40vh")
		})

		it("Should have columns", () => {
			expect(tree).toHaveStyleRule("grid-template-columns", "repeat(10,1fr)")
		})

		it("Should have gutter", () => {
			expect(tree).toHaveStyleRule("grid-column-gap", "17px")
		})

		it("Should have padding", () => {
			expect(tree).toHaveStyleRule("padding", "21px")
		})
	})

})