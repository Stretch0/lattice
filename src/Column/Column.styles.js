import styled from "styled-components"
import { ifProp } from "styled-tools"
import span from "../utils/span"

const Column = styled("div")`
	
  ${span}

  ${ifProp(
    "justifyContent",
    ({ justifyContent }) => `
      display: flex;
      flex-wrap: wrap;
      justify-content: ${justifyContent};
    `
  )}

  ${ifProp(
    "flexDirection",
    ({ flexDirection }) => `
      flex-direction: ${flexDirection}
    `
  )}

	${ifProp("verticalAlign", ({ verticalAlign }) => `align-items: ${verticalAlign}`)}

  ${ifProp("alignItems", ({ alignItems }) => `align-items: ${alignItems}`)}

	${ifProp("outline", () => `outline: solid red 1px;`)}
`

export default Column
