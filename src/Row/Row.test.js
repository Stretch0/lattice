import React from 'react'
import styled from 'styled-components'
import renderer from 'react-test-renderer'
import Row from "./Row.styles"
import 'jest-styled-components'

test('Row default styles', () => {
  const tree = renderer.create(<Row />).toJSON()
  expect(tree).toHaveStyleRule('display', 'grid')
  expect(tree).toHaveStyleRule('height', 'min-content')
  expect(tree).toHaveStyleRule('grid-template-columns', 'repeat(12,1fr)')
  expect(tree).toHaveStyleRule('grid-column-gap', '8px')
  expect(tree).toHaveStyleRule('margin', '8px 4px')
  expect(tree).toHaveStyleRule('padding', '8px 4px')
  expect(tree).toHaveStyleRule('grid-column', undefined)
})