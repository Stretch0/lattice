import PropTypes from "prop-types"
import styled from "styled-components"
import { ifProp } from "styled-tools"
import calcSpan from "../utils/span"

const Row = styled("div")`
	display: grid;
	height: ${({height}) => height || "min-content"};

	grid-template-columns: ${({ theme: { layout }, columns }) => `repeat(${columns || layout.columns}, 1fr)`};
  
  grid-column-gap: ${({ theme: { layout }, gap }) => gap || layout.gap};

  margin: ${({ theme: { layout }, margin }) => margin ? margin.sm : margin || layout.margin};
  padding: ${({ theme: { layout }, padding }) => padding || layout.padding};

	${ifProp("alignItems", ({ alignItems }) => `align-items: ${alignItems}`)};

	${ifProp("outline", () => `outline: solid blue 1px;`)};

	> * {
		grid-column: 1 / -1;
	}
	
	${calcSpan}

  ${({span, start}) => calcSpan({span: span && span.xs, start: start && start.xs})}

	@media (min-width: 600px) {
  	${({span, start}) => calcSpan({span: span && span.sm, start: start && start.sm})}
    margin: ${({margin}) => margin ? margin.sm : margin};
  }
	
  @media (min-width: 900px) {
  	${({span, start}) => calcSpan({span: span && span.md, start: start && start.md})}
    margin: ${({margin}) => margin ? margin.md : margin};
  }

  @media (min-width: 1200px) {
  	${({span, start}) => calcSpan({span: span && span.lg, start: start && start.lg})}
    margin: ${({margin}) => margin ? margin.lg : margin};
  }

  @media (min-width: 1800px) {
  	${({span, start}) => calcSpan({span: span && span.xl, start: start && start.xl})}
    margin: ${({margin}) => margin ? margin.xl : margin};
  }
`

Row.propTypes = {
  height: PropTypes.string,
  columns: PropTypes.number,
  gap: PropTypes.string,
  alignItems: PropTypes.oneOf(["start", "end", "center", "stretch"]),
  outline: PropTypes.string,
  padding: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.shape({
      xs: PropTypes.string,
      sm: PropTypes.string,
      md: PropTypes.string,
      lg: PropTypes.string,
      xl: PropTypes.string
    })
  ]),
  margin: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.shape({
      xs: PropTypes.string,
      sm: PropTypes.string,
      md: PropTypes.string,
      lg: PropTypes.string,
      xl: PropTypes.string
    })
  ]),
  span: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.shape({
      xs: PropTypes.string,
      sm: PropTypes.string,
      md: PropTypes.string,
      lg: PropTypes.string,
      xl: PropTypes.string
    })
  ]),
  start: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.shape({
      xs: PropTypes.string,
      sm: PropTypes.string,
      md: PropTypes.string,
      lg: PropTypes.string,
      xl: PropTypes.string
    })
  ])
}

Row.defaultProps = {
  theme: {
    layout: {
      columns: 12,
      gap: "8px",
      margin: "8px 4px",
      padding: "8px 4px"
    }
  }
}

export default Row