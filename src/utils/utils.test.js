import span from "./span"

describe("Span", () => {

	it("Should return grid-column span", () => {
		expect(span({span: 12})).toEqual(["grid-column: span ", "12"])
	})

	it("Should return grid-column span & start", () => {
		expect(span({span: 6, start: 6})).toEqual(["grid-column: ", "6", " / span ", "6"])
	})

	it("Should NOT return grid-column", () => {
		expect(span({start: 6})).toEqual(undefined)
	})


})