import { css } from "styled-components"

const calcSpan = ({span, start}) => {

	if(span && parseInt(span)){
		if (start && parseInt(start)) {
      return css`grid-column: ${start} / span ${span}`
    }else{
    	return css`grid-column: span ${span}`
    }
	}
	
}

export default calcSpan